import numpy as np
import matplotlib.pyplot as plt


# ADD CODE: read in the data here 
data = np.loadtxt("signal_data.txt")
t = data[:,0]
f = data[:,1]

dt = t[1]-t[0]
N=len(t)

#plt.plot(t,f)

# Fourier coefficients from numpy fft normalized by multiplication of dt
F = np.fft.fft(f)*dt
F2 = F.copy()
# frequencies from numpy fftfreq
freq = np.fft.fftfreq(len(F),d=dt)

# inverse Fourier with numpy ifft (normalization removed with division by dt)
iF = np.fft.ifft(F/dt)

# !!! This for loop didnt work but the condition below worked ??
#for fre in freq:
#    if (fre<40 or fre>60):
#        F2[fre] = 0

F2[(freq<40)] = 0
F2[(freq>60)] = 0   

iF2 = np.fft.fftfreq(len(F2),d = dt)           
# positive frequencies are given as
# freq[:N//2] from above or freq = np.linspace(0, 1.0/dt/2, N//2)

fig1, ax1 = plt.subplots(2)
# plot over positive frequencies the Fourier transform
ax1[0].plot(freq[:N//2], np.abs(F[:N//2]))
ax1[1].plot(freq[:N//2], np.abs(F2[:N//2]))
ax1[0].legend(["$f$ (Hz)",])
ax1[1].legend(["cut off $f$ (Hz)",])
#ax.ylabel(r'$F(\omega/2\pi)$')
 
# plot the "signal" and test the inverse transform
fig, ax = plt.subplots(3)
ax[0].plot(t, f,'r--')
ax[1].plot(t,iF.real,'r--')
ax[2].plot(t,iF2.real,'r--')
ax1[0].legend(["Real signal",])
ax1[1].legend(["Signal thorugh fouriers",])
ax1[2].legend(["Signal after filtering only 40-50hz",])
#ax.set_xlabel(r'$t$ (s)')
#ax.set_ylabel(r'$f(t)$')

plt.show()
