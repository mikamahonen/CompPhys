# -*- coding: utf-8 -*-
"""
Created on Thu Feb  3 19:41:54 2022

@author: mikam
"""

from numpy import *
import numpy as np
from scipy.integrate import simps
from read_xsf_example import read_example_xsf_density

def main():
    filename = 'dft_chargedensity1.xsf'
    rho, lattice, grid, shift = read_example_xsf_density(filename)

    
    x = np.linspace(0,lattice[0][0],grid[0])
    y = np.linspace(0,lattice[1][1],grid[1])
    z = np.linspace(0,lattice[2][2],grid[2])
    X,Y,Z = meshgrid(x,y,z)
    electrons = simps(simps(simps(rho,x,axis = 0),y,axis =0),z)
    
    vec = [lattice[0][0],lattice[1][1],lattice[2][2]]
    vec = vec/(max(vec))
    print("electrons 1")
    print(electrons)
    
    
    rho, lattice, grid, shift = read_example_xsf_density('dft_chargedensity2.xsf')
    
    #J = A, latticevectors
    A = np.transpose(lattice)
    I = np.identity(3)
    
    Bt = np.transpose(2*np.pi*np.dot(I,np.linalg.inv(A)))
    
    alpha1 = np.linspace(0,1,grid[0])
    alpha2 = np.linspace(0,1,grid[1])
    alpha3 = np.linspace(0,1,grid[2])
    alpha = [alpha1, alpha2, alpha3]
    
    
    eee = simps(simps(simps(rho,alpha1,axis=0),alpha2,axis = 0),alpha3)*np.linalg.det(A)
    print("electron number 2",eee)
    print("Lattice vectors in not cubic case")
    print(Bt)
    
    
    
    x = np.linspace(0,lattice[0][0],grid[0])
    y = np.linspace(0,lattice[1][1],grid[1])
    z = np.linspace(0,lattice[2][2],grid[2])
    X,Y,Z = meshgrid(x,y,z)
    # take modulo 1 of alphas
    electrons = simps(simps(simps(rho,x,axis=0),y,axis = 0),z)
    
    vec = [lattice[0][0],lattice[1][1],lattice[2][2]]
    vec = vec/(max(vec))
    print("electrons 2 by normal integration")
    print(electrons)
    
    
    
    
if __name__=="__main__":
    main()