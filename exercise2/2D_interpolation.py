# -*- coding: utf-8 -*-
"""
Created on Thu Jan 27 13:55:35 2022

@author: mikam
"""
import numpy as np
import matplotlib as plt

xexp = np.loadtxt("x_grid.txt")
yexp = np.loadtxt("y_grid.txt")
Fexp = np.loadtxt("exp_data.txt")
Fexp = Fexp.reshape([len(xexp),len(yexp)])

X,Y = np.meshgrid(xexp,yexp)
plt.figure()
plt.contourf(X.T,Y.T,Fexp)
plt.scatter(X.T,Y.T,color="red")
plt.show()
