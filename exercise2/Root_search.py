# -*- coding: utf-8 -*-
"""
Created on Sun Jan 23 12:33:53 2022

@author: mikam
"""

import numpy as np

def find_zeros(f,test_f,grid,N):
    # f : array of the data
    # test_function : Used for more precise calc.
    # grid : Array grid used for f and used for more precise calc.
    # N : The num points used in calculating points in between grid
    
    # Two options: first one is that the dim ==0
    # second is that the dimension is bigger and the function calls are different
    # Both should work. The container types are just different
    zeros = []
    dimensions = f.ndim-1
    print(dimensions)
    dim = 0
    
    if dim ==0:
        val =0
        while val < len(f)-1:
            #print(val)
            # check is there zero in between these points
            if (f[val]*f[val+1])>0:
                val +=1
            else:     
                # check if the end points are zeros
                if f[val] ==0:
                    #print("here 2")
                    zeros.add[grid[val]]
                    val +=1
                    print(val)
                elif f[val+1] ==0:
                    zeros.add[grid[val+1]]
                    #print("here 3")
                    val +=1
                    print(val)
                else:
                    # Findind the zero in between the grid points
                    a = grid[val]
                    b = grid[val+1]
                    print(val)
                    a1 = a # variables for comparing data
                    b1 = b
                    # N is the num points used
                    for n in range(1,N+1):
                        a2 = (a + b)/2
                        if dimensions ==0:
                            f_a2 = test_f(a2)
                        else:
                            f_a2 = f(dim,a2)
                        if test_f(a1)*f_a2<0:
                            a1 = a1
                            b1 = a2
                        elif f_a2*test_f(b1)<0:
                            a1 = a2
                            b1 = b1
                        else:
                            return None
                        zeros.append((a1+b1)/2)
                        val +=1
    else:
        while dim <= dimensions: 
            #print("here 1")
            val =0
            while val < len(f)-1:
                #print(val)
                # check is there zero in between these points
                if (f[dim][val]*f[dim][val+1])>0:
                    val +=1
                else:     
                    # check if the end points are zeros
                    if f[dim][val] ==0:
                        #print("here 2")
                        zeros.add[grid[dim][val]]
                        val +=1
                        print(val)
                    elif f[dim][val+1] ==0:
                        zeros.add[grid[dim][val+1]]
                        #print("here 3")
                        val +=1
                        print(val)
                    else:
                        # Findind the zero in between the grid points
                        a = grid[dim][val]
                        b = grid[dim][val+1]
                        print(val)
                        a1 = a # variables for comparing data
                        b1 = b
                        # N is the num points used
                        for n in range(1,N+1):
                            a2 = (a + b)/2
                            if dimensions ==0:
                                f_a2 = test_f(a2)
                            else:
                                f_a2 = f(dim,a2)
                            if test_f(a1)*f_a2<0:
                                a1 = a1
                                b1 = a2
                            elif f_a2*test_f(b1)<0:
                                a1 = a2
                                b1 = b1
                            else:
                                return None
                            zeros.append((a1+b1)/2)
                            val +=1      
                dim +=1
        
    return zeros
def test_f2(x):
    return x**2 - x - 1
    
def main():
    x = np.linspace(-10, 10,80)
    f = test_f2(x)
    print(f[0])
    test_f = lambda x: x**2-x-1
    zeros =find_zeros(f,test_f,x,2)
    print("zeros"+str(zeros))
    
    # dim = 1
    # rmax = 100 
    # r0 = 1e-5
    # dim = 100
    # div = 100-1
    # h = (rmax/r0+1)/div
    # r = [0]*100
    #for i in range(1,dim):
        #r[i]=r0*(np.exp(i*h)-1)
    
    #end main
if __name__=="__main__":
    main()