# -*- coding: utf-8 -*-
"""
Created on Sun Jan 23 20:53:38 2022

@author: mikam
"""
import numpy as np

def find_deepest_descent(fun,fundot,start,a,N,tol):
    # fun : lambda functio 
    # fundot : derivative of fun, also given by lambda
    # start : start point for iteration
    # a : iteration step
    # N : max amount of steps
    # tol : tolerance when the program stops
    # Should work for N dimensions if the fun and fundot are changed and x is matrix
    x = start
    for num in range(N):
        
        gamma = -a*fundot(x)
        
        if np.all(np.abs(gamma))<= tol:
            break
        
        x += gamma
    return x
    
# def fun(x):
#     return lambda x:x**2
# def fundot(x):
#     return lambda x: 2*x

    
def main():
    #fun2 = lambda x : x.sum(axis=1)
    fun = lambda x:x**2
    fundot = lambda x: 2*x
    pos =find_deepest_descent(fun,fundot,1,0.01,1000,1e-3)
    # Good to notice that the N value needs to be big if step is small 
    print(str(pos))

if __name__=="__main__":
    main()    