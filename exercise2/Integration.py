# -*- coding: utf-8 -*-
"""
Created on Thu Jan 20 18:11:53 2022

@author: mikam
"""
import numpy as np
import math
from scipy.integrate import trapz
from scipy.integrate import simps
from scipy.integrate import quad
from scipy.integrate import nquad
from numpy import sin
from numpy import exp


num_points = [10, 20, 40, 100, 200, 400]

def choosefunction(number,x):
    if number == 0:
        y = integrand1a(x)
    if number == 1:
        y = integrand1b(x)
    if number == 2:
        y = integrand1c(x)
    return y

def integrand1a(x):
    return x**2*exp(-2*x)*np.sin(2*x)

def integrand1b(x):
    return np.sin(x)/x

def integrand1c(x):
    return exp(np.sin(x**3))

def main():

    trapz_results = [[],[],[]]
    limits = [[0,1],[1e-10,1],[0,5]]
    
    for num in num_points:
        kierros = 0
        while kierros <3: 
            x = np.array([limits[kierros][0],1/num,limits[kierros][1]])
            y = choosefunction(kierros,x)
            I = trapz(y, x)
            trapz_results[kierros].append(I)
            kierros +=1;
    
    simpson_results = [[],[],[]]
    
    for num in num_points:
        kierros = 0
        while kierros <3:
            x = np.array([limits[kierros][0],1/num,limits[kierros][1]])
            y = choosefunction(kierros,x)
            I = simps(y, x)
            simpson_results[kierros].append(I) 
            kierros +=1;
            
    def expint(n, x):
        return quad(integrand1a, 0, np.inf)[0]
    
    inte1 = lambda x: x**2*exp(-2*x)*np.sin(2*x)
    inte2 = lambda x: np.sin(x)/x
    inte3 = lambda x: exp(np.sin(x**3))
    I1 = quad(inte1, 0, np.inf)
    I2 = quad(inte2, 0, 1)
    I3 = quad(inte3, 0, 5)
    quad_results = [I1[0],I2[0],I3[0]]
    

    print("I 1)")
    print("trapz results  "+" simpson results "+"   num points \n")
    for index in [0,1,2,3,4,5]: 
        
        print(str(trapz_results[0][index])+" "+ str(simpson_results[0][index])+"   "+str(num_points[index])+"\n")
        
    print("Quad results: "+str(quad_results[0])+"\n")

    print("I 2)")
    print("trapz results  "+" simpson results "+"   num points \n")
    for index in [0,1,2,3,4,5]: 
        
        print(str(trapz_results[1][index])+" "+ str(simpson_results[1][index])+"   "+str(num_points[index])+"\n")
        
    print("Quad results: "+str(quad_results[1])+"\n")

    print("I 3)")
    print("trapz results  "+" simpson results "+"   num points \n")
    for index in [0,1,2,3,4,5]: 
        
        print(str(trapz_results[2][index])+" "+ str(simpson_results[2][index])+"   "+str(num_points[index])+"\n")
        
    print("Quad results: "+str(quad_results[2])+"\n")

    print("Two dimensional")
    
    def twodimensfun(x,y):
        return x*exp(-np.sqrt(x**2+y**2))
    
    x = np.linspace(0, 2, 100)
    y = np.linspace(-2, 2, 100)
    X, Y = np.meshgrid(x,y) 
    fun = twodimensfun(X, Y)
    two_dim_inte_trapz = trapz(trapz(fun,x),y) 
    two_dim_inte_simpson = simps(simps(fun,x),y)
    two_dim_inte_nquad = nquad(twodimensfun, [[0,2],[-2,2]])
    
    print(str(two_dim_inte_trapz)+" "+str(two_dim_inte_simpson)+"\n ")
    print(str(two_dim_inte_nquad[0]))
    
    def threedimensfun(x,y,z):
        return ((1/np.sqrt(np.pi))*(exp(-np.sqrt((x+1)**2+y**2+z**2))+exp(-np.sqrt((x-1)**2+y**2+z**2))))**2
    
    x = np.linspace(-10, 10, 100)
    y = np.linspace(-10, 10, 100)
    z = np.linspace(-10, 10, 100)
    X, Y, Z = np.meshgrid(x,y,z)

    fun3 = threedimensfun(X, Y, Z)
    three_dim_inte_trapz = trapz(trapz(trapz(fun3,x),y),z) 
    three_dim_inte_simpson = simps(simps(simps(fun3,x),y),z)
    three_dim_inte_nquad = nquad(threedimensfun, [[-10,10],[-10,10],[-10,10]])
    
    print("1C) Three dim")
    print(str(three_dim_inte_trapz)+" "+str(three_dim_inte_simpson)+"\n ")
    print(str(three_dim_inte_nquad[0]))
    
if __name__=="__main__":
    main()
    
        