# -*- coding: utf-8 -*-
"""
Created on Sun Jan 23 17:38:55 2022

@author: mikam
"""
import numpy as np 

def numerical_gradient(f,N,x0,dx):
    # f : Array of N dimensions
    # np gradient gives the gradient of the matrix
    df = np.zero[N]
    for i in range(0,N):
        ddx = np.zeros(N)
        ddx[i] = dx
        df[i] = (f(x0+ddx,N)-f(x0-ddx,N))/(2*ddx)
    
    
    return df
      
def f(x,N):
    fun = np.sin(x[0])+np.cos([N-1])
    for i in range(0,N):
        fun = fun +x[i]**2
    return fun        

def test_f(x,N):
    
    fun = np.zeros(N)
    fun[0] = x[2]*np.cos([0])+x[2]*x[1]**2
    fun[1] = np.cos([0])+x[2]*x[1]**2
    fun[2] = x[2]*np.cos([0])+x[2]*x[1]**2
    
    return fun



def main():
    # i am creating matrix manually just to test the functionality
    x = np.linspace(0,50,100)
    y = np.linspace(0,50,100)
    z = np.linspace(0,50,100) 
    matrix = np.matrix([x,y,z])
    results = numerical_gradient(test_f(matrix))
    print(results)

if __name__=="__main__":
      main()  
  
    