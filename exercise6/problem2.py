# -*- coding: utf-8 -*-
"""
Created on Thu Feb 17 19:13:11 2022

@author: mikam
"""
import numpy as np
import matplotlib.pyplot as plt

def make_matrix(x):
    N = len(x)
    h = x[1]-x[0]
    off_diag = np.ones((N-1,))
    A = np.zeros((N,N)) - np.diag(off_diag,-1) + np.diag(off_diag,1)
    A[0,0:3] = [-3.0,4.0,-1.0] 
    A[-1,-3:] = [1.0,-4.0,3.0] 
    return A/h/2

def poisson(x,N):
       #h = 1/N
       #A1 = make_matrix(x[1:N-1])
       A = makeA(x[1:N-1],N)
       phi = np.zeros((N-2,1))
       b = makeb(x[1:N-1],N) #[1:N-1]
       #rho = np.pi**2*np.sin(np.pi*x[1:N-1])*h**2
       #phi = np.linalg.solve(A, rho.T)
       print("A")
       print(A)
       print("b")
       print(b)
       phi = np.linalg.solve(A, b)
       phi_n = np.concatenate((phi,[0]))
       phi_n = np.concatenate(([0],phi_n))
       return phi_n   

def makeA(x,N):
    siz = np.size(x)
    print(siz)
    A = np.zeros(shape = (N,N))
    A = np.zeros(shape = (siz,siz))
    h = 1/(N-1)
    for index1 in range(siz): # N
        for index2 in range(siz): # N
            if index1 == index2:
                A[index1][index2] = 2/(h)
            if index1 == index2+1:
                A[index1][index2] = -1/(h)
            if index1 == index2-1:
                A[index1][index2] = -1/(h)      
    return A

def makeb(x,N):
    
    b = np.zeros_like(x)
    h = 1/(N-1)
    for i in range(1,np.size(x)-1):   
        b[i] = np.pi/h*(x[i-1]+x[i+1]-2*x[i])*np.cos(np.pi*x[i])+1/h*(2*np.sin(np.pi*x[i])-np.sin(np.pi*x[i-1])-np.sin(np.pi*x[i+1]))
    return b

def main():
    N = 10
    x = np.linspace(0,1,N)
    sol = poisson(x,N)
    acc = np.sin(np.pi*x)
    print("sol")
    print(sol)
    print("acc")
    
    print(acc)
    # = np.linspace(0,1,N+2)
    plt.plot(x,sol,color = "blue")
    plt.plot(x,acc,color = "red")
    plt.show()
    
if __name__=="__main__":
    main()