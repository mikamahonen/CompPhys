# -*- coding: utf-8 -*-
"""
Created on Fri Feb 18 12:50:43 2022

@author: mikam
"""

import numpy as np
import scipy.integrate as sp
import matplotlib.pyplot as plt

def make_matrix(x):
    N = len(x)
    h = x[1]-x[0]
    off_diag = np.ones((N-1,))
    A = np.zeros((N,N)) - np.diag(off_diag,-1) + np.diag(off_diag,1)
    A[0,0:3] = [-3.0,4.0,-1.0] 
    A[-1,-3:] = [1.0,-4.0,3.0] 
    return A/h/2

def poisson(x,N):
       #h = 1/N
       #A1 = make_matrix(x[1:N-1])
       A = makeA(x[1:N-1],N)
       Ahat = makeAhat(x)
       phi = np.zeros((N-2,1))
       b = makeb(x,N)
       #bhat = makebhat(x[1:N-1],N)
       #rho = np.pi**2*np.sin(np.pi*x[1:N-1])*h**2
       #phi = np.linalg.solve(A, rho.T)
       print(A)
       print(b)
       phi = np.linalg.solve(A, b.T)
       phi_n = np.concatenate((phi,[0]))
       phi_n = np.concatenate(([0],phi_n))
       return phi_n
   
def makeAhat(x):
    siz = np.size(x)
    u = np.zeros_like(x)
    A = np.zeros(shape = (siz,siz))
    h = x[1]-x[0]
    for i in range(1,np.size(x)-1):
        u[i] = sp.quad(right_hand_side,x[i-1],x[i+1],args=(x[i],h))[0]
        for j in range(1,np.size(x)-1):
            if (abs(i-j)<2):
                xmin = np.min([x[max([0,i-2])],x[max([0,j-2])]])
                xmax = np.max([x[min([len(x)-1,i+2])],x[min([len(x)-1,j+2])]])
                A[i,j] = sp.quad(left_hand_side,xmin,xmax,args=(x[i],x[j],h))[0]
    return A
def hat_fun(x,xi,h):   
    if x<xi:
      return (x-xi)/h
    else:
     return (xi-x)/h

def der_hat_fun(x,xi,h):
       
   if x<xi:
      return 1/h
   else:
      return 1/h 

def left_hand_side(x,xi,xj,h):
    return der_hat_fun(x,xi,h)*der_hat_fun(x,xj,h)

def right_hand_side(x,xi,h):
    return hat_fun(x,xi,h)*np.pi**2*np.sin(np.pi*x)

def makebhat(x,N):
    
    b = np.zeros(shape = (N,N))
    h = x[1]-x[0]
    for i in range(1,np.size(x)-1):
        b[i] = sp.quad(right_hand_side, x[i-1],x[i+1],args=(x[i],h))[0]
    return b


       
def makeA(x,N):
    siz = np.size(x)
    print(siz)
    A = np.zeros(shape = (N,N))
    A = np.zeros(shape = (siz,siz))
    h = 1/(N-1)
    for index1 in range(siz): # N
        for index2 in range(siz): # N
            if index1 == index2:
                A[index1][index2] = 2/(h)
            if index1 == index2+1:
                A[index1][index2] = -1/(h)
            if index1 == index2-1:
                A[index1][index2] = -1/(h)      
    return A

def makeb(x,N):
    
    b = np.zeros_like(x)
    h = 1/(N-1)
    for i in range(1,np.size(x)-1):   
        b[i] = np.pi/h*(x[i-1]+x[i+1]-2*x[i])*np.cos(np.pi*x[i])+1/h*(2*np.sin(np.pi*x[i])-np.sin(np.pi*x[i-1])-np.sin(np.pi*x[i+1]))
    return b

def main():
    N = 10
    x = np.linspace(0,1,N)
    sol = poisson(x,N)
    acc = np.sin(np.pi*x)
    print(sol)
    # = np.linspace(0,1,N+2)
    plt.plot(x,sol,color = "blue")
    plt.plot(x,acc,color = "red")
    plt.show()
    
if __name__=="__main__":
    main()