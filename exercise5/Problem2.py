# -*- coding: utf-8 -*-
"""
Created on Wed Feb  9 23:11:44 2022

@author: mikam

Also here the code is done but there is some mistakes so that the program doesnt 
produce the wanted outcome.
Mybe the eq is somehow wrong not sure and I have to try to do the other questions
too so no time to investigate :(
"""

import numpy as np
from matplotlib.pyplot import *

from scipy.integrate import odeint
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

def runge_kutta4(x,t,dt,func,**kwargs):
    """
    Fourth order Runge-Kutta for solving ODEs
    dx/dt = f(x,t)

    x = state vector at time t
    t = time
    dt = time step

    func = function on the right-hand side,
    i.e., dx/dt = func(x,t;params)
    
    kwargs = possible parameters for the function
             given as args=(a,b,c,...)

    See Computational Physics 1 lecture notes.
    """
    F1 = F2 = F3 = F4 = 0.0*x
    if ('args' in kwargs):
        args = kwargs['args']
        F1 = func(x,t)
        F2 = func(x+dt/2*F1,t+dt/2)
        F3 = func(x+dt/2*F2,t+dt/2)
        F4 = func(x+dt*F3,t+dt)
    else:
        F1 = func(x,t)
        F2 = func(x+dt/2*F1,t+dt/2)
        F3 = func(x+dt/2*F2,t+dt/2)
        F4 = func(x+dt*F3,t+dt)

    return x+dt/6*(F1+2*F2+2*F3+F4), t+dt

def eq(x,t):
    qE = [0.05,0,0]
    qB = [0,4,0]
    v = x[3:5]
    newstate = np.zeros_like(x)
    newstate[0:3]=np.array(qE+np.cross(v,qB))
    newstate[3:6] = x[0:3]
    #newstate[3] = x[3]
    #newstate[4] = x[4]
    #newstate[5] = x[5]
    # dv/dt = qE+ v x qB
    return np.array(newstate)

def equationsofmotion(x,t):
    qE = [0.05,0,0]
    qB = [0,4,0]
    return eq(x, t)

def calc_motion(x0,v0,tmin,tmax,N):
    
    dt = (tmin-tmax)/N
    t = np.linspace(tmin,tmax,N)
    x = np.zeros((N,3))
    statevec = x0+v0
    sol=[]
    statevec=1.0*np.array(statevec)
    for i in range(len(t)):
        sol.append(statevec)
        statevec, tp = runge_kutta4(statevec,t[i],dt,eq)
        
    sol=np.array(sol)
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    print(sol)
    print(np.shape(sol))
    sol = np.transpose(sol)
    print(np.shape(sol))
    x = sol[0]
    y = sol[1]
    z = sol[2]
    ax.plot(x,y,z)
    
    


def main():
    tmin = 0
    tmax = 5
    N = 1000
    x0 = [0,0,0]
    v0 = [0.1,0.1,0.1]
    qE = [0.05,0,0]
    qB = [0,4,0]
    
    
    calc_motion(x0, v0, tmin, tmax, N)
    
    # fig=figure()
    # ax1=fig.add_subplot(131)
    # ax2=fig.add_subplot(132)
    # ax3=fig.add_subplot(133)
    # solve_ivp_test(ax1)
    # ax1.set_title('solve_ivp')
    # odeint_test(ax2)
    # ax2.set_title('odeint')
    # runge_kutta_test(ax3)
    # ax3.set_title('own Runge-Kutta 4')
    # show()
    


if __name__=="__main__":
    main()