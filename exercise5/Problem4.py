# -*- coding: utf-8 -*-
"""
Created on Sun Feb 13 00:00:58 2022

@author: mikam

All update methos done but missing parts due to a lack of time
Tolerance is missing
comparing the convergence is missing
"""
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def jacobi(Phi,rho,h,N):
    Phi_new = 1.0*Phi
    for i in range(1,N-1):      
        for j in range(1,N-1):   
            Phi_new[i,j] = 0.25*(Phi[i+1,j]+Phi[i-1,j]+Phi[i,j+1]+Phi[i,j-1]+h**2*rho[i,j])
    return Phi_new

def Gauss_Seidel(Phi,rho,h,N):
    Phi_new = 1.0*Phi
    for i in range(1,N-1):      
        for j in range(1,N-1):   
            Phi_new[i,j] = 0.25*(Phi[i+1,j]+Phi_new[i-1,j]+Phi[i,j+1]+Phi_new[i,j-1]+h**2*rho[i,j])
    return Phi_new

def SOR(Phi,rho,h,N):
    Phi_new = 1.0*Phi
    omega = 1.8
    for i in range(1,N-1):      
        for j in range(1,N-1):   
            Phi[i,j] = (1-omega)*Phi[i,j]+omega/0.25*(Phi[i+1,j]+Phi_new[i-1,j]+Phi[i,j+1]+Phi_new[i,j-1]+h**2*rho[i,j])
    return Phi_new



def main():
    
    L = 1.0
    h = 0.05
    N = np.int(L/h+1)
    x = np.linspace(0.,L,N)
    y = np.linspace(0.,L,N)
    rho = np.zeros((N,N))
    Phi = np.zeros((N,N))
    X,Y = np.meshgrid(x,y)
    Phi[0,:] = 1.0
    
    fig = plt.figure()
    
    ax1 = fig.add_subplot(111, projection="3d")
    ax1.plot_wireframe(np.transpose(X),np.transpose(Y),Phi,rstride=1,cstride=1)
    
    loops = 1.0
    I = False
    tol = 1.0e-2
    while I== False:
        Phi_old = 1*Phi
        #Phi = jacobi(Phi,rho,h,N)
        Phi = Gauss_Seidel(Phi,rho,h,N)
        loops +=1
        if( loops>100):
            I=True
            
    print(loops)
    fig = plt.figure()
    
    ax = fig.add_subplot(111, projection="3d")
    ax.plot_wireframe(np.transpose(X),np.transpose(Y),Phi,rstride=1,cstride=1)
    

if __name__=="__main__":
    main()