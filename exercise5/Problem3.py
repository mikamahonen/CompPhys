# -*- coding: utf-8 -*-
"""
Created on Fri Feb 11 12:19:54 2022

@author: mikam

Somehow i got stuck here. I have mistake in the formulas but i cant figure out where
I made two derivative methods to compare them but the problem is somewhere else
maybe in the f-function

"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.linalg import toeplitz
from scipy.interpolate import UnivariateSpline



def main():
    N = 10
    x = np.linspace(0,1,N+1)
    dx = (x[1]-x[0])
    
    e_0 = 1
    f = e_0*np.pi**2*np.sin(np.pi*x)/13
    
    sol = np.sin(np.pi*x)
    rho = np.zeros_like(f)
    #for i in range(np.size(x)-2):
       # rho[i+1] = f[x[i+1]]
    
    abound = 0
    bbound = 1
    rho[0] = abound
    rho[np.size(x)-1] = bbound
    
    firstrow = np.zeros_like(x)
    firstcol = np.zeros_like(x)
    firstrow[0] = 2
    firstrow[1] = -1
    firstcol[0] = 2
    firstcol[1] = -1
    A = toeplitz(firstcol,firstrow)
    
    A = A/dx**2
    # First derivative method
    N = len(x)
    h = x[1]-x[0]
    off_diag = np.ones((N-1,))
    derivativematrix = np.zeros((N,N)) - np.diag(off_diag,-1) + np.diag(off_diag,1)
    derivativematrix[0,0:3] = [-3.0,4.0,-1.0] 
    derivativematrix[-1,-3:] = [1.0,-4.0,3.0] 
    derivativematrix= derivativematrix/h/2
    derivation = -np.dot(derivativematrix,np.dot(derivativematrix,f))
    derivation[0] = 0
    derivation[N-1] = 0
    #print(derivation)
    ## Second derivation method
    i = 1
    der2 = np.zeros_like(x)
    while i< len(x)-1:
        der2[i] = -(f[i-1]-2*f[i]+f[i+1])/dx**2
        #print(i,der2)
        i +=1
    
    der2[0] =0
    der2[N-1] =0
    print("A matrix")
    
    print(A)
    print("derivative 2")
    print(der2)
    #print(derivation)
    res = np.linalg.solve(A,derivation)
    res2 = np.linalg.solve(A,der2)
    err = np.max(np.abs(res2-sol))
    print("Error: ")
    print(err)
    plt.plot(x,res)
    plt.plot(x,res2)
    plt.plot(x,sol)
    plt.show()
    
if __name__=="__main__":
    main()