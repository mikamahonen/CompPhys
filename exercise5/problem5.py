# -*- coding: utf-8 -*-
"""
Created on Sun Feb 13 17:54:28 2022

quiver command didnt produce nice pictures so streamplot was used instead
This is not actually what was asked but kinda as close as i could get the
wanted result.

I asume the quiver needed some scaling options and parameters
You can try to uncomment it and try.
"""

import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as lines


WIDTH, HEIGHT, DPI = 700, 700, 100

def E(q, r0, x, y):
    # Calculate the E field 
    den = ((x-r0[0])**2 + (y-r0[1])**2)**1.5
    return q * (x - r0[0]) / den, q * (y - r0[1]) / den

# Grid 
nx, ny = 100, 100
x = np.linspace(-2, 2, nx)
y = np.linspace(-2, 2, ny)
X, Y = np.meshgrid(x, y)

# Create a capacitor
d =  0.6
Q = 20;
charges = []
for i in range(Q):
    charges.append((0.3, (i/(Q-1)*0.7-0.3, -d/2)))
    charges.append((-0.3, (i/(Q-1)*0.7-0.3, d/2)))


Ex, Ey = np.zeros((ny, nx)), np.zeros((ny, nx))
for charge in charges:
    ex, ey = E(*charge, x=X, y=Y)
    Ex += ex
    Ey += ey

fig = plt.figure(figsize=(WIDTH/DPI, HEIGHT/DPI))

ax = fig.add_subplot()



color = np.log(np.sqrt(Ex**2 + Ey**2))
ax.streamplot(x, y, Ex, Ey, color=color, linewidth=1,
              density=2, arrowstyle='->')


#widths = np.linspace(0, 2, X.size)
# Normal
#ax.quiver(X, Y, Ex,Ey,color)

# Try to scale with log
#ax.quiver(X, Y, np.log10(np.abs(Ex)),np.log10(np.abs(Ey)),color)

xdata = np.linspace(-d/2,d/2,100)
ydata = 0.3*np.ones_like(xdata)
ax.plot(xdata,-1*ydata,linestyle='solid',color='blue',linewidth='8')
ax.plot(xdata,ydata,linestyle='solid',color='red',linewidth='8')
ax.set_xlabel('$x$')
ax.set_ylabel('$y$')
ax.set_xlim(-2,2)
ax.set_ylim(-2,2)
ax.set_aspect('equal')
plt.savefig('capacitor.png', dpi=DPI)
plt.show()