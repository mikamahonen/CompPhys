# -*- coding: utf-8 -*-
"""
Created on Mon Jan 31 08:56:27 2022

@author: mikam

Doesn´t work 
    unit vector and vector magnitude is the problem
"""
import numpy as np
from scipy.integrate import simps

def diff_electric(x):
    e_0 = 8.8541878128e-12
    k = (1/(4*np.pi*e_0))
    L = 2
    Q = 1e-9
    d = 1
    dx = x[1]-x[1]
    lamb = Q/L
    r_0 = [L/2+d,0];
    d = 1
    y = 0;
    y_0 = 0;
    mag = ((r_0[0]-x)**2+(r_0[1]-y)**2)
    unit_vector = np.linalg.norm(x)
    k2 = (lamb*dx/mag)*unit_vector
    return k*k2
def main():
    L = 2
    d = 1
    x = np.linspace(-L/2,L/2,200);
    E = simps(diff_electric(x),x)
    print(E)
    
if __name__=="__main__":
    main()