# -*- coding: utf-8 -*-
"""
Created on Thu Jan 27 13:55:35 2022

@author: mikam
"""
import numpy as np
import math
import matplotlib.pyplot as plt
from spline_class import spline as sp

xexp = np.loadtxt("x_grid.txt")
yexp = np.loadtxt("y_grid.txt")
Fexp1 = np.loadtxt("exp_data.txt")
Fexp = Fexp1.reshape([len(xexp),len(yexp)])

x_new = np.linspace(0,1,100)
y_new = np.linspace(0,1,100)

f = np.zeros_like(x_new)

x_old = np.linspace(0, 1,5)
f_old = np.zeros_like(x_old)

fig, axs = plt.subplots(2)

axs[0].plot(x_new,2*x_new**2)

for i in range(len(x_new)):
    
    spl2d=sp(x = xexp,y = yexp,f = Fexp,dims=2)
    f[i]=spl2d.eval2d(x_new[i],2*x_new[i]**2)
    
for i in range(len(x_old)):
    
    spl2d=sp(x = xexp,y = yexp,f = Fexp,dims=2)
    f_old[i]=spl2d.eval2d(x_old[i],2*x_old[i]**2)    
    
plt.plot(x_old,f_old)

X,Y = np.meshgrid(xexp,yexp)
#X_new,Y_new = np.meshgrid(new_x,new_y)

axs[1].plot(x_new,f)
#plt.scatter(x_new,spl2d)




axs[0].contourf(X.T,Y.T,Fexp)
axs[0].scatter(X.T,Y.T,color="red")
#axs[0].scatter(x_new,f,color = "black")
#plt.show()
