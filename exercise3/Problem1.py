# -*- coding: utf-8 -*-
"""
Created on Sat Jan 29 22:12:22 2022

@author: mikam
"""

from scipy.integrate import simps,nquad
import numpy as np


def fun(x,y):
    return (x+y)*np.exp(-np.sqrt(x**2+y**2)/4)

def main():

    x = np.linspace(0, 2, 500)
    y = np.linspace(-2, 2, 500)
    X, Y = np.meshgrid(x,y)
    
    two_dim_inte_simpson = simps(simps(fun(X,Y),x),y)
    two_dim_inte_nquad = nquad(fun, [[0,2],[-2,2]])
    
    print(two_dim_inte_simpson)
    print(two_dim_inte_nquad[0])
    
    
    
if __name__=="__main__":
    main()