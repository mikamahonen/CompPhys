import numpy as np
import matplotlib.pyplot as plt
from scipy import sparse


def make_matrix(x):
    N = len(x)
    h = x[1]-x[0]
    off_diag = np.ones((N-1,))
    A = np.zeros((N,N)) - np.diag(off_diag,-1) + np.diag(off_diag,1)
    A[0,0:3] = [-3.0,4.0,-1.0] 
    A[-1,-3:] = [1.0,-4.0,3.0] 
    return A/h/2
    
    """
    Normal: f´(x) = (f(x+h)-f(x-h))/(2h)
                    
    Edge: f´(x) = (4*f(x+h)-f(x+2h)-3*f(x))/(2h)
                    so terms in A  -3,4,-1
    """
    

def main():
    N = 50
    grid = np.linspace(0,np.pi,N)
    x = np.sin(grid)
    x2 = np.cos(grid)
    x3 = np.exp(grid)
    x4 = grid**5
    
    A = make_matrix(grid)
    
    # calculate here b=Ax as described in the problem setup
    b = np.dot(A,x)
    b2 = np.dot(A,x2)
    b3 = np.dot(A,x3)
    b4 = np.dot(A,x4)
    
    fig, axs = plt.subplots(4)
    
    axs[0].plot(grid,b,grid,x,'--',color = "red")
    axs[1].plot(grid,b2,grid,x2,'--',color = "blue")
    axs[2].plot(grid,b3,grid,x3,'--',color = "black")
    axs[3].plot(grid,b4,grid,x4,'--',color = "Yellow")
    

if __name__=="__main__":
    main()



