"""
Computational physics 1

1. Add code to function 'largest_eig'
- use the power method to obtain the largest eigenvalue and the 
  corresponding eigenvector of the provided matrix

2. Compare the results with scipy's eigs
- this is provided, but you should use that to validating your 
  power method implementation

Notice: 
  dot(A,x), A.dot(x), A @ x could be helpful for performing 
  matrix operations

"""


import numpy as np
import matplotlib.pyplot as plt
import scipy.sparse as sp
import scipy.sparse.linalg as sla
from scipy.integrate import simps


def largest_eig(A,tol=1e-9):
    """
    - Add simple power method code in this function
    - Add appropriate commenting espcially in this comment section
    Calculates the eigenvalues of a matrix and returns the eigenvector and the largest value
    HUOM!!
        This works with all of my examples but somehow the data in A just doesnt
        go through
        What's wrong?? Truth value is ambigous but i couldnt solve it.
    """
    init = np.ones((A.shape[0]),dtype = int)
    x = init
    x2 = np.array([1, 1])
    n = 0
    a=0
    a_old = 0
    N = 5000
    while True:
        x = np.dot(A,x) 
        a, x = normalize(x)
        # if a stabilazes the loop ends
        if n>0 and a-a_old <tol:
            False
        if n>N:
           break
        a_old = a
        n +=1
      
    return a, x

def normalize(x):
    """
    Idea is to normalize x here and return normalized matrix and laergest value a
    Parameters
    ----------
    x : Matrix

    Returns
    -------
    fac : Largest value in x
    x : normalized matrix

    """
    fac = abs(x)
    fac = max(fac)
    x = x / x.max()
    return fac, x

def main():
    grid = np.linspace(-5,5,100)
    grid_size = grid.shape[0]
    dx = grid[1]-grid[0]
    dx2 = dx*dx
    
    H0 = sp.diags(
        [
            -0.5 / dx2 * np.ones(grid_size - 1),
            1.0 / dx2 * np.ones(grid_size) - 1.0/(abs(grid)+1.5),
            -0.5 / dx2 * np.ones(grid_size - 1)
        ],
        [-1, 0, 1])
    
    A = np.array([[0, 2],[2, 3]])
    a, vec = largest_eig(A)
    print(a)
    
    eigs, evecs = sla.eigsh(H0, k=1, which='LA')

    A = np.array([[0.9,10.2,0.7],[3.3,0.9,1.2],[0.01,7.4,2.5]],float)
    a, vec = largest_eig(A)
    print(a)
    
    l,vec=largest_eig(H0)
    
    print('largest_eig estimate: ', l)
    print('scipy eigsh estimate: ', eigs)
    
    psi0=evecs[:,0]
    norm_const=simps(abs(psi0)**2,x=grid)
    psi0=psi0/norm_const
    print(norm_const)

    psi0_ = vec*1.0
    norm_const=simps(abs(psi0_)**2,x=grid)
    psi0_=psi0_/norm_const
    print(norm_const)

    plt.plot(grid,abs(psi0)**2,label='scipy eig. vector squared')
    plt.plot(grid,abs(psi0_)**2,'r--',label='largest_eig vector squared')
    plt.legend(loc=0)
    plt.show()


if __name__=="__main__":
    main()
