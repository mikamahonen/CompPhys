# -*- coding: utf-8 -*-
"""
Created on Sun Feb 20 16:57:08 2022

@author: mikam


Makes two animations of  Time-dependent Schrödinger equation in 1D

In first the potentian is zero and in second one the is a potential well

When running the print in console is just to indicate that the simulation is running
The tinker can sometimes be unresponsive, just running the program again can help
please move the windows so that they dont overlap because that causes flickering

Things that doesnt work that i intended;
    -labels in tinker doesnt work
    -I had to use sparse for making A due to its big scase i asume
    -Also I tried implementing other integration methods but 
        integrate.solve_ivp() was the only one i got working
    -I tried also making better animation but also in this case tinker
        was only working one
"""

import numpy as np
from scipy import integrate
from scipy import sparse
import matplotlib.pyplot as plt
from matplotlib import animation
plt.matplotlib.use('TkAgg')

plt.rc('savefig', dpi=300)


# Constants chosen just to fit this simulation

N = 1000; dx = 10/N;k0 = 50;m = 1;sigma = 0.5;x0 = 3.5                   
x = np.linspace(0, 10, N)   
                   
# Wavefunction

psi0 = np.sqrt(1.0/(sigma*np.sqrt(np.pi)))*np.exp(-(x-x0)**2/(2.0 * sigma**2))*np.exp(1j*k0*x)

# Potential well used 
V_hight = 2000

V = np.zeros(x.shape)
i = 0
for _x in x:
    if _x > 5 or _x<2.5:
        V[i] = V_hight
    i =i+1    
V2 = np.zeros(x.shape)

# !Huom have to use this method for A because the normal ones doesnt work 
# in this scale of points

A = sparse.diags([1, -2, 1], [-1, 0, 1], shape=(N,N))
A = A/dx**2
#print(D2)

"""
Here these are just tries for alternative methods

#off_diag = np.ones((N-1,))
#D2 = np.zeros((N,N)) - np.diag(off_diag,-1) + np.diag(off_diag,1)
#D2 =D2/dx**2

# def makeA(x,N):
#     siz = np.size(x)
#     print(siz)
#     A = np.zeros(shape = (N,N))
    
#     h = 10/N
#     for index1 in range(siz): # N
#         for index2 in range(siz): # N
#             if index1 == index2:
#                 A[index1][index2] = 2/(h)**2
#             if index1 == index2+1:
#                 A[index1][index2] = -1/(h)**2
#             if index1 == index2-1:
#                 A[index1][index2] = -1/(h)**2      
    
#     return A

#D2 = makeA(x,N)

"""
def runge_kutta4(x,t,dt,func,**kwargs):
    """
    Fourth order Runge-Kutta for solving ODEs
    dx/dt = f(x,t)

    x = state vector at time t
    t = time
    dt = time step

    func = function on the right-hand side,
    i.e., dx/dt = func(x,t;params)
    
    kwargs = possible parameters for the function
             given as args=(a,b,c,...)

    See Computational Physics 1 lecture notes.
    """
    F1 = F2 = F3 = F4 = 0.0*x
    if ('args' in kwargs):
        args = kwargs['args']
        F1 = func(x,t)
        F2 = func(x+dt/2*F1,t+dt/2)
        F3 = func(x+dt/2*F2,t+dt/2)
        F4 = func(x+dt*F3,t+dt)
    else:
        F1 = func(x,t)
        F2 = func(x+dt/2*F1,t+dt/2)
        F3 = func(x+dt/2*F2,t+dt/2)
        F4 = func(x+dt*F3,t+dt)

    return x+dt/6*(F1+2*F2+2*F3+F4), t+dt

hbar = 1
def psi_v0(t, psi):
    return -1j * (- 0.5 * hbar / m * A.dot(psi))

def psi_v0_Runge(psi,t):
    return -1j * (- 0.5 * hbar / m * A.dot(psi))


def psi_V(t, psi):
    return -1j * (- 0.5 * hbar / m * A.dot(psi) + V / hbar * psi)

def Psi(x,t):
    alpha = sigma**2+1j*hbar*t/m
    potens = sigma**4/alpha**2 * (x-x0-hbar*k0*t/m)**2/sigma**2
    Psi = sigma/(alpha*np.sqrt(np.pi))*np.exp(-potens)
    return Psi
    
    
# Solve the equations with V=0 and V = 2000

sol = integrate.solve_ivp(psi_V,t_span=[0.0, 0.3], y0=psi0 ,t_eval=np.arange(0.0, 0.3, 0.0005), method="RK23")

sol2 = integrate.solve_ivp(psi_v0,t_span=[0.0, 0.1], y0=psi0 ,t_eval=np.arange(0.0, 0.1, 0.0005), method="RK23")

# statevec = psi0
# t3=np.arange(0.0, 0.1, 0.0005)
# sol3 = []
# for i in range(len(t3)):
#     sol3.append(statevec)
#     statevec, tp = runge_kutta4(statevec,t3[i],0.0005,psi_v0_Runge)
    
# sol3.append(statevec)

# for i, t in enumerate(sol3):
#     plt.clf()
#     plt.plot(x, V*0.001, "k--", label=r"$V(x) (x0.01)")
#     plt.plot(x, np.abs(sol3.y[:, i])**2,color='blue',label="Absolute amplitude of the psi")
#     plt.plot(x, np.real(sol3.y[:, i]),color='Red',label="Real amplitude of the psi")  
#     plt.pause(0.1)
#     print("plot 2 going on")


# Plotting

fig = plt.figure()
for i, t in enumerate(sol2.t):
    plt.clf()
    plt.plot(x, np.abs(sol2.y[:, i])**2,color='blue',label="Absolute amplitude of the psi")
    plt.plot(x, np.real(sol2.y[:, i]),color='Red',label="Real amplitude of the psi")  
    plt.pause(0.05)
    print("plot 1 going on")
    


fig = plt.figure()
for i, t in enumerate(sol.t):
    plt.clf()
    plt.plot(x, V*0.001, "k--", label=r"$V(x) (x0.01)")
    plt.plot(x, np.abs(sol.y[:, i])**2,color='blue',label="Absolute amplitude of the psi")
    plt.plot(x, np.real(sol.y[:, i]),color='Red',label="Real amplitude of the psi")  
    plt.pause(0.1)
    print("plot 2 going on")




plt.show()
