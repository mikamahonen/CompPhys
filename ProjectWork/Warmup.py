# -*- coding: utf-8 -*-
"""
Created on Sun Feb 27 12:07:58 2022

@author: mikam
"""
import numpy as np
from scipy.integrate import simps
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D


def fun(x,y):
    return (x+3*y)*np.exp(-0.5*np.sqrt(x**2+y**2))
    
    
def main():
    r0 = np.array([1.3,0.2])
    r1 = np.array([0.45,1])

    lattice= np.array([r0,r1])
    A = np.transpose(lattice)
    A = np.array([[1.3,0.2],[0.45,1]]).T
    N = 11
    alpha = np.linspace(0,1,N)
    RES = np.zeros((N,N))
    
    for i in range(N):
        for j in range(N):
            a = np.array([alpha[i],alpha[j]])
            r = A.dot(a)
            
            RES[i,j] = fun(r[0],r[1])
            
            
    integ = simps(simps(RES,alpha),alpha)*np.linalg.det(A)
    x = np.linspace(0, 2,50)
    y = np.linspace(0, 2,50)
    
    X,Y = np.meshgrid(x,y)
    
    Z = fun(X,Y)
    fig,ax=plt.subplots(1,1)
    cp = ax.contourf(X, Y, Z)
    fig.colorbar(cp) # Add a colorbar to a plot
    ax.set_title('Contour plot')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    
    x1 = [0,1.3]
    y1 = [0,0.2]
    line1 = Line2D(x1, y1)
    x2 = [0,0.45]
    y2 = [0,1]
    line2 = Line2D(x2, y2)
    ax.add_line(line1)
    ax.add_line(line2)
    plt.show()
    print(integ)
if __name__=="__main__":
    main()