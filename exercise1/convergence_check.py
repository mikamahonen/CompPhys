# -*- coding: utf-8 -*-
"""
Created on Fri Jan 14 12:26:42 2022

@author: mikam, basis copied from CompPhys 

The goal is to print two results in to same figure

Integration is made with monte carlo method and the derivatio with first
aproksimation of derivation. The accuracy is increasing with smaller dx in 
derivatio and increasing the iterations in integration.

# Sin(0.5) is used as an example
"""

import numpy as np
import matplotlib.pyplot as plt

from num_calculus import monte_carlo_integration
from num_calculus import first_derivative

# creation of test function sin(x)
def fun(x):
    return np.sin(x)

# Creation of two arrays where data can be saved
derivative_results = [0]*100
integral_results = [0]*100
integral = 0.877582561890372716116 # This i

# loops for calculating the results
for x in range(len(derivative_results)):
    dx = 0.5/(x+1)
    derivative_results[x] = integral-first_derivative(fun, 0.5, dx)
    # theoretical - approximation
    
for x in range(len(integral_results)):
    n = x+1
    integral_results[x] = 2-monte_carlo_integration(fun, 0, np.pi,10,n)[0]    
    # theoretical - approximation


plt.rcParams["legend.handlelength"] = 2
plt.rcParams["legend.numpoints"] = 1
plt.rcParams["text.usetex"] = False # I couldn't make the latex work
plt.rcParams["font.size"] = 12

fig = plt.figure()
# - or, e.g., fig = plt.figure(figsize=(width, height))
# - so-called golden ratio: width=height*(np.sqrt(5.0)+1.0)/2.0
# - width and height are given in inches (1 inch is roughly 2.54 cm)
ax = fig.add_subplot(111)
#x = np.linspace(0,np.pi/2,100)
x = np.linspace(-50,5,100)
f = np.sin(x)
g = np.exp(-5*x)
# plot and add label if legend desired
ax.set_title("Error of sin(x) derivate at 0.5 and integral from 0 to 2 pi. ")
ax.plot(x,g,label=r"derivate of sin()")

#ax.plot(x,integral_results,"--",label=r"Monte carlo integration")
# include legend (with best location, i.e., loc=0)
ax.legend(loc=0)

# set axes labels and limits
ax.set_xlabel(r"dx = 1/x, number of iterations") # , dx=(1/(2x)) and iterations n=x
ax.set_ylabel(r"$Error$")
ax.set_xlim(x.min(), x.max())
fig.tight_layout(pad=1)

# save figure as pdf with 200dpi resolution
fig.savefig("testfile.pdf",dpi=200)
plt.show()
