""" Course CompPhys, exercise 1"""

import numpy as np

def first_derivative( function, x, dx ):
	# function: inserted function
	# x: scalar 
	# dx derivation step
	return (function(x+dx)-function(x-dx))/(2*dx)

def second_derivative(function,x,dx):
	# function: inserted function
	# x: scalar 
	# dx derivation step
	return (function(x+dx)+function(x-dx)-2*function(x))/(dx**2)

def simpson_int(function,x):
    # f, input function 
    # x, array
    # returns the integral value
    
	h = x[1]-x[0]
	n = len(x)
    # Two cases, odd number of block or even number
    # If even then we must calculate the last piece differently
	if (n % 2) == 0:
		I_simp_odd = (h/12) * (-function[n-3] + 8*(function[n-2]) \
				+ 5*(function[n-1]))
		
		I_simp_even = (h/3) * (function[0] + 2*sum(function[:n-2:2]) \
				+ 4*sum(function[1:n-1:2]) + function[n-2])

		I_simp = I_simp_even + I_simp_odd
	else: 
		I_simp = (h/3) * (function[0] + 2*sum(function[:n-2:2]) \
				+ 4*sum(function[1:n-1:2]) + function[n-1])
	return I_simp

def Lefthand_Rieman_sum(f,x):
	# f, input function 
    # x, array
    # returns the integral value
    
	h = x[1]-x[0]  
	n = len(x)     # Number of steps
	integ = 0      # starts form zero
	#left_riemann_sum = np.sum(f(x) * h)
	for d in n-1:
		integ += h*f[d]
	return integ 

def trapezoid_sum(f,x):
	# f, input function 
    # x, array
    # returns the integral value
    
	h = x[1]-x[0]  # uniform step size
	n = len(x)
	integ = 0

	for d in n-1:
		integ += h*(f(d)+f(d+1))
		
	return integ

def monte_carlo_integration(fun,xmin,xmax,blocks=10,iters=100):
	# fun, input function 
    # xmin, start value
    # xmax, end value
    # blocks, number of blocks assumed to be 10
    # iters, iteration number 
    # returns the integral value
    
	block_values=np.zeros((blocks,))
	L=xmax-xmin
	for block in range(blocks):
		for i in range(iters):
			x = xmin+np.random.rand()*L
			block_values[block]+=fun(x)
		block_values[block]/=iters
	I = L*np.mean(block_values)
	dI = L*np.std(block_values)/np.sqrt(blocks)
	return I,dI

def func(x):
	return np.sin(x)


def test_first_derivative():
	
	# manually used function to test if first_derivative works
	x1 = 1.5
	x2 = 3
	h = 0.01
	numerical_estimate1 = first_derivative(function1,x1,h)
	exact1 = exact_derivate(x1)
	error1 = numerical_estimate1-exact1
	numerical_estimate2 = first_derivative(function1,x2,h)
	exact2 = exact_derivate(x2)
	error2 = numerical_estimate2-exact2
	
	if (abs(error1)<1e-2 and abs(error2)<1e-2):
		print("first derivative okey")
	else:
		print("first derivative not good")

def test_second_derivative():
    # manually used function to test if second_derivative works
    
	x2 = 3
	h = 0.01
	numerical_estimate1 = second_derivative(function1,x2,h)
	exact1 = exact_second_derivative(x2)
	error = numerical_estimate1-exact1

	if (abs(error)<1e-2):
		print("second derivative okey")
	else:
		print("second derivative not good")

def test_integral(numerical, exact, method):
	# Test function
	# numerical: float which holds the value of the numerical integral
	# exact: theoretical valuea of the integral
	# method: name of the integration method, used in printing

	if (abs(numerical)-abs(exact))<0.05: 
		print("Integral by "+method+" okay")
		
	else:
		print("Integral by"+method+"not accurate enough")
		

def function1(x):
    # Test function
    
	return 5*x**2

def function2(x):
    # Test function
    
	return 2*x**3

def exact_derivate(x):
	# Test function, solution
    
	return 10*x
	
def exact_second_derivative(x):
    # Test function, solution
    
	return 10

def main():

	first_derivative(func, 0.5, 0.01)
    
	x = np.linspace(0,np.pi/2,101)
	f = np.sin(x)
	I = simpson_int(f,x)
	test_integral(I,1,"simpson")

	I,dI=monte_carlo_integration(func,0.,np.pi/2,10,100)
	print("Integrated value:, {0:0.5f} +/- {1:0.5f}".format(I,2*dI))
	test_integral(I,1,"monte carlo")

if __name__=="__main__":
	main()
